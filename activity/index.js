const number = 2
const getCube = 2**3;
console.log(`The cube of ${number} is ${getCube}`);

const address = ["258 Washington Ave NW", "California", 90011]
const [primaryAddress, state, postalCode] = address;
console.log(`I live at ${primaryAddress}, ${state}, ${postalCode}`);

const animal = {
    name: "Lolong",
    species: "saltwater crocodile",
    weight: 1075,
    measurement: "20 ft 3 in"
};

const {name, species, weight, measurement} = animal
console.log(`${name} was a ${species}. He weighed at ${weight} kgs with a measurement of ${measurement}.`)

const numbers = [1,2,3,4,5]
numbers.forEach(x => console.log(x))

const numAdd = (accumulator, curr) => accumulator + curr;
console.log(numbers.reduce(numAdd));

class Dog{
    constructor(name, age, breed){
        this.name = name,
        this.age = age,
        this.breed = breed
    }
};

const doggy = new Dog("Frankie", 5, "Miniature Daschund");
console.log(doggy);