// ECMAScript is the standard that is used to create implementations of the language, one of which is JavaScript.

// ES6 updates brings new features

// S19 is to focus on basic updates from ES6
// This allows us to write less and do more.

// Exponent Operator
	// pre-ES6
    const firstNum = Math.pow(8, 2);
    console.log(firstNum);
// ES6
    const secondNum = 8**2;
    console.log(secondNum);


// Template Literals - allows writing of strings without the use of concat operator;
/*
MINIACTIVITY
    -create a name variable and  store a name of a person
    -create a message, greeting the person and welcoming him/her in the programming field
    -log in the console the message
*/

// pre-ES6
    let name = "John";
    let message = "Hello " + name + "! Welcome to the programming field!";
    console.log(message);
// ES6
    message = `Hello ${name}! Welcome to the programming field!`;
    console.log(message);

// Multiline
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8**2 with the answer of ${secondNum}`;
console.log(anotherMessage);

// Computation inside the template literals
const interestRate = 0.1;
const principal = 1000;
console.log(`The interest on your savings is ${principal*(1+interestRate)}`);

/*
MINIACTIVITY 2
    -create an array that has firstName, middleName, and lastName elements
    -login the console each element (1 row - 1 element)
*/



// Array Destructuring - allows unpacking elements in arrays into distinct variables; allows naming of array elements with variable instead of using index numbers; helps iwth the code readability and coding efficiency.
/*
SYNTAX:
    let/const [variableA, variableB, variableC, ... variableN] = arrayName
*/

// pre-ES6
const fullName = ["Juan", "Dela", "Cruz"];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}`);

// ES6
const [ firstName, middleName, lastName ] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);



// Object Destructuring
/*
create an object that contains a woman's givenName, maidenName, familyName
log in the console each key (1 row - 1 key-value pair)
*/ 

// pre-ES6
let women = {
givenName: "Maria",
maidenName: "Maharlika",
familyName: "Clara"
};
console.log(women.givenName)
console.log(women.maidenName)
console.log(women.familyName)

// ES6
const {givenName, maidenName, familyName} = women;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);

// Arrow Function
/* 
    create a function that displays/logs a person's firstName, middleInitial, and lastName in the console.
*/
// pre-es6
function printFullName(firstName, middleInitial, lastName){
    console.log(firstName);
    console.log(middleInitial);
    console.log(lastName);
    console.log(firstName, middleInitial, lastName);
};

printFullName("Jane", "R.", "Rivera");


// es6
const printFName = (fname, mname, lname) =>{
    console.log(fname, mname, lname);
};

printFName("Will", "D.", "Smith");

const students = ['John', 'Jane', 'Joe']

// pre-es6
students.forEach(
    function(pick){
        console.log(pick);
    }
);
// post-es6
students.forEach(x => console.log(x));

// pre-es6
function add(x,y){
    return x + y;
};
let total = add(1,2);
console.log(total);

// es6
const adds = (x,y) => x+y;
let totals = adds(4,6);
console.log(totals);

// (Arrow Function) Default Function Argument Value

const greet = (name = "User") =>{
    return `Good morning, ${name}`
};
console.log(greet());

// once the function has specified parameter value
console.log(`Result of specified value for parameter ${greet("John")}`);

// 
/* 
    create a car object using the object method
    car field: name, brand, year
*/

/* function car(name, brand, year){
    this.name = name
    this.brand = brand
    this.year = year
};

const car1 = new car("Subaru", "Forester", 2020);
console.log(car1); */

// class constructor
class car{
    constructor(brand, name, year){
        this.brand = brand,
        this.name = name,
        this.year = year
    }
};

const car1 = new car("Subaru", "Forester", 2020);
console.log(car1);

const car2 = new car();
car2.brand = "Nissan";
car2.name = "Terra";
car2.year = 2022;
console.log(car2);

/* 
    create a variable that stores a number
    create an if-else statement using ternary operator
    condition = if the number is <= 0, return true, if it is > 0, return false
*/

let num = 10;

(num <= 0) ? console.log(true) : console.log(false);